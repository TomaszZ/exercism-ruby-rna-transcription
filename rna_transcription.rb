module BookKeeping
     VERSION = 4
end

class Complement
  def self.of_dna(dna_strand)
    compl = String.new
    dna_strand.each_char do |strand|
      compl.concat(self.find_complement(strand))
    end

    # if compl contains whitespace ( as a result of 
    # wrong dna_strand query) return empty string
    compl =~ / / ? '' : compl 
    
  end
  
  def self.find_complement(input)
    case input
    when 'C'
      'G'
    when 'G'
      'C'
    when 'T'
      'A'
    when 'A'
      'U'
    else
      ' '
    end
  end
end